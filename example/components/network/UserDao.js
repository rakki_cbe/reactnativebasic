import React, { useEffect, useState } from 'react';
function getdata(setData,setLoading){

    fetch('http://10.0.2.2:8080/users')
    .then((response) => response.json())
    .then((json) => setData(json.data.users))
    .catch((error) => console.error(error))
    .finally(() => setLoading(false));

}

function authendicateUser(auth,setLoading,setResult){
    fetch('http://10.0.2.2:8080/login',{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic '+auth//QW5zb25fbmV3MjI6cGFzc3dvcmQ='
        },
        body: JSON.stringify({
            name:'Inserted from react',
            userName:'React2',
            password:'password'
            }),
      })
    .then((response) => response.json())
    .then((json) => {
       if(json.responseCode =="1111")
            setResult(json.responseCode);
        else
            setResult("0000");
    })
    .catch((error) => {
        setResult("0000")
        //console.error(error)
    }
    )
    .finally(() => setLoading(false));
}

function creatUser(data,setLoading,setResult){
    fetch('http://10.0.2.2:8080/createuser',{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
      })
    .then((response) => response.json())
    .then((json) => {
       if(json.responseCode =="1111")
            setResult(json.responseCode);
        else
            setResult("0000");
    })
    .catch((error) => {
        setResult("0000")
        //console.error(error)
    }
    )
    .finally(() => setLoading(false));
}


export {getdata,authendicateUser,creatUser}