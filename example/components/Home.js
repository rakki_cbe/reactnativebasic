import * as React from 'react';
import { Text, View } from 'react-native';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import Login from './login/Login'
import Sucess from './login/Sucess'
import Network from './Users/ShowUsers'
import CreateUser from './login/RegisterUser'

const Stack = createStackNavigator() 
function Home() {
  return (
    <NavigationContainer>
    <Stack.Navigator>
        <Stack.Screen
        name="Login"
        component={Login}
        options={{title: 'Welcome'}}
        />
         <Stack.Screen
        name="Sucess"
        component={Sucess}
        options={{title: 'Sucess'}}
        />
         <Stack.Screen
        name="Network"
        component={Network}
        options={{title: 'Network'}}
        />
        <Stack.Screen
        name="Create"
        component={CreateUser}
        options={{title: 'Create User'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}



export default Home;

