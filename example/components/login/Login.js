import * as React from 'react'
import {ActivityIndicator,Text,Button,TextInput,View,ScrollView} from 'react-native'
import {loginStyle} from './LoginStyle'
import {isFourmValid} from './LoginFunction'
import {authendicateUser} from '../network/UserDao'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {decode as atob, encode as btoa} from 'base-64'



export default function Login({ navigation }){
    const[userName,setUserName] = React.useState("");
    const[password,setPassword] = React.useState("");
    const[isValid,setValid]=React.useState(false)
    const [isLoading, setLoading] = React.useState(true);
    const [sucessCode, setLoginSucessCode] = React.useState("");
    if (sucessCode=="1111") {
        navigation.navigate("Network")
        setLoginSucessCode("")
    } else if(sucessCode=="0000") {
        navigation.navigate("Sucess",{sucess:false})
        setLoginSucessCode("")
    }
    return(
        <ScrollView style={loginStyle.scrollView}>
            <View style ={loginStyle.container}>
                <Text style={loginStyle.title}>Login</Text>
                    <View style={loginStyle.inputTextContainer}>
                    <Text style={loginStyle.inputTitle}>UserName :</Text>
                        <TextInput style={loginStyle.inputText}
                            placeholder =""
                            onChangeText={text => {
                                setUserName(text)
                                setValid(isFourmValid(userName,password))
                            }
                        }
                            defaultValue = {userName}/>
                    </View>
                    <View style={loginStyle.inputTextContainer}>
                    <Text style={loginStyle.inputTitle}>Password :</Text>
                    <TextInput style={loginStyle.inputText}
                        placeholder =""
                        onChangeText={text => {
                            setPassword(text)
                            setValid(isFourmValid(userName,password))
                        }
                        }
                        secureTextEntry = {true}
                        defaultValue = {password}/>
                    </View>
                    <View style={loginStyle.buttonContainer}>
                        <Button style={{width: 150, height: 50}} 
                        title="Clear" onPress ={()=> {
                            setUserName("");
                            setPassword("");
                        }} />
                        <Button style={{width: 150,height: 50}}
                        onPress ={()=>{
                            const auth = btoa(userName + ':' + password);
                                authendicateUser(auth,setLoading,setLoginSucessCode)
                    
                        }
                    }
                    disabled= {!isValid}
                        title="Submit"/>
                    </View>
                    <Button title="Create User" 
                    onPress={()=> navigation.navigate("Create")}/>
                    
            </View>
        </ScrollView>
    );
}