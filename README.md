# React installation  #

1. Node Js 
2. homebrew (/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
3. Watchman 
4. npm install -g react-native-cli (Sudo if you get access error)
5. react-native init EmojiDictRN (EmojiDictRN - name of your project)
6  react-native start - Will initialise server to looking for changes (There is possible error for default port 8081 so we need to run 
React-native start —port=8088)

---

### Files Explanation in that folder  ###

** App.js ** the first file in any React Native app that is the entry point of the app development process. Whatever you write inside this file, it will get displayed on the mobile device.

** node_modules/ ** is a folder which contains all the dependencies (or packages) that are used to develop and run this application.
 
** index.js ** is the entry point to trigger the app on a device or simulator

** ios ** is the folder containing an Xcode project and the code required to bootstrap this app for iOS devices

** android ** is the folder containing android related code to bootstrap this app for Android devices

** package.json ** where every dependency installed gets listed
--- 

### Building and Running ###

* react-native run-android - To build and install apk to default emulator 
* --simulator="iPhone 8 Plus" this will help you to install to specific targeted device 
* There is possibility for error if you changed your port in step 6  so to rectify it 
    * Step 1 - Press CMD + M on you simulator 
    * Step -2 Press Settings 
    * Step -3  Enter 10.0.2.2:8088(port number given on step 6)
    * Step-4 Re-run app using react-native run-android
---

### Reacts fundamental ###

* components
* JSX
* props
* state
** Components ** 
 Either class or function in react everything you see in screen is components 

** JSX ** 
  Its like HTML tags we can used scripts in side those tags by {}

** Props **
	Immutable variable only readable used to initialise component   State
    Mutable variable can use two ways function also class 

## Understanding Props and State ## 

What's the difference between state and props in React?
In a React component, the props are the variables that we pass from a parent component to a child component. Similarly, the state are also variables, with the difference that they are not passed as parameters, but rather that the component initializes and manages them internally.

Text , TextInput,Button,View,Scrollview,FlatList, SectionList.


Write platform specific code 
Platform.os Platform.select platform.version 
Specify your file with platform specific extension like name.ios.js or name.android.js


Using React-native with existing app
 https://reactnative.dev/docs/integration-with-existing-apps

---

## To run this ##

 ** Delete the node_modules folder and any 'lock' files such as 
yarn.lock or package-lock.json if present. **

** npm install **

### Who do I talk to? ###

* Radhakrishnan (rakki.bsc2009@gmail.com)
* Freelancer 