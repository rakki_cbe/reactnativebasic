/**
 * @format
 */
import {AppRegistry} from 'react-native';
//import App from './example/components/propsExample';
//import App from './example/components/helloWorld';
//import App from './example/components/stateExample';
import App from './example/components/Home';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
